import { useState, useEffect, useRef } from 'react';

const MyCollapse = ({ title, content }) => {
  const [isOpen, setIsOpen] = useState(false);

  const collapse = useRef(null);

  const handleClick = () => {
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    // Set up our click handler
    const handleClickOutside = (event) => {
      if (!collapse.current.contains(event.target)) {
        setIsOpen(false);
      }
    };

    // Make sure collapse.current isn't null still
    if (!!collapse.current) {
      // Add the listener when we open, take it away when we close
      if (isOpen) {
        window.addEventListener('click', handleClickOutside);
      } else {
        window.removeEventListener('click', handleClickOutside);
      }
    }

    // If this component is destroyed, remove that event listener
    return () => {
      window.removeEventListener('click', handleClickOutside);
    };
  }, [isOpen, collapse]);

  return (
    <div ref={collapse}>
      <button onClick={handleClick}>{title}</button>
      {isOpen && (
        <div>
          <p>{content}</p>
        </div>
      )}
    </div>
  );
};

export default MyCollapse;
