// Global
import type { NextPage, GetStaticPaths, GetStaticProps } from 'next';
import { ComponentPropsContext, SitecoreContext } from '@sitecore-jss/sitecore-jss-nextjs';
import { ApolloProvider } from '@apollo/client';
// Lib
import { sitecorePagePropsFactory } from 'lib/sitecore/page-props-factory';
import { sitemapFetcher } from 'lib/sitecore/sitemap-fetcher';
import graphQLClientFactory from 'lib/graphql/client-factory';
import type { ExtendedSitecoreContext } from 'lib/sitecore/sitecore-context';
import type { SitecorePageProps } from 'lib/sitecore/page-props';
// Config
import { componentFactory } from 'temp/component-factory';
// Components
import PageLayout from 'components/layout/PageLayout';
import NotFound from 'components/layout/NotFound';

export const getStaticPaths: GetStaticPaths = async (context) => {
  if (process.env.NODE_ENV !== 'development') {
    const paths = await sitemapFetcher.fetch(context);

    return {
      paths,
      fallback: process.env.EXPORT_MODE ? false : 'blocking',
    };
  }

  return {
    paths: [],
    fallback: 'blocking',
  };
};

export const getStaticProps: GetStaticProps = async (context) => {
  const props = await sitecorePagePropsFactory.create(context);

  // Revalidate the ISR cache via the supplied environment variable
  // or once every 300 seconds (5 minutes)
  const revalidate = !!process.env.ISR_REVALIDATE_TIMER
    ? parseInt(process.env.ISR_REVALIDATE_TIMER, 10)
    : 300;

  return {
    props,
    revalidate,
    notFound: props.notFound,
  };
};

const SitecorePage: NextPage<SitecorePageProps> = ({ componentProps, layoutData, notFound }) => {
  // NotFound should be caught/served as part of getStaticPaths but this is
  // here as a backup just in case.
  if (notFound || !layoutData?.sitecore?.route) {
    return <NotFound />;
  }

  const graphQLClient = graphQLClientFactory();

  return (
    <ComponentPropsContext value={componentProps}>
      <SitecoreContext layoutData={layoutData} componentFactory={componentFactory}>
        <ApolloProvider client={graphQLClient}>
          <PageLayout />
        </ApolloProvider>
      </SitecoreContext>
    </ComponentPropsContext>
  );
};

export default SitecorePage;
