// Global
const {
  jssJestTemplate,
  jssMockDataTemplate,
  jssStorybookTemplate,
  reactComponent,
  jssScaffoldOpts,
} = require('@dullaghan/cli-scaffold-templates');
// Local
const authorableComponent = require('./scripts/scaffold/templates/authorable-component.js');

module.exports = {
  projectType: 'JSS',
  scaffold: {
    subdirectories: [
      {
        path: './components/authorable/general',
        name: 'General',
        dataComponent: 'authorable/general/',
        storybook: 'Authorable/General/',
      },
      {
        path: './components/authorable/hero',
        name: 'Hero',
        dataComponent: 'authorable/hero/',
        storybook: 'Authorable/Hero/',
      },
      {
        path: './components/authorable/layout',
        name: 'Layout',
        dataComponent: 'authorable/layout/',
        storybook: 'Authorable/Layout/',
      },
      {
        path: './components/authorable/listing',
        name: 'Listing',
        dataComponent: 'authorable/listing/',
        storybook: 'Authorable/Listing/',
      },
      {
        path: './components/authorable/promo',
        name: 'Promo',
        dataComponent: 'authorable/promo/',
        storybook: 'Authorable/Promo/',
      },
      {
        path: './components/authorable/site',
        name: 'Site',
        dataComponent: 'authorable/site/',
        storybook: 'Authorable/Site/',
      },
      {
        path: './components/helpers',
        name: 'Helpers',
        dataComponent: 'helpers/',
        storybook: 'Helpers/',
        templates: {
          '[name].tsx': reactComponent,
        },
      },
    ],
    templates: {
      '[name].mock-data.js': jssMockDataTemplate,
      '[name].stories.jsx': jssStorybookTemplate,
      '[name].test.js': jssJestTemplate,
      '[name].tsx': authorableComponent,
    },
    scaffoldOpts: jssScaffoldOpts,
  },
};
