const defaultData = {
  field: { value: { src: '/mocks/test.jpg', alt: '', height: '900', width: '1600' } },
};

export const layoutFill = {
  field: {
    value: {
      src: '/mocks/layout-fill.png',
      alt: '',
    },
  },
  layout: 'fill',
};

export const noContent = {
  field: {},
};

export default defaultData;
