const IconPlus = (): JSX.Element => (
  <path d="M27.5 4v16.5H44v7H27.5V44h-7V27.5H4v-7h16.5V4h7z" fill="currentColor" />
);

export default IconPlus;
