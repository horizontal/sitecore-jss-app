// Global
import { useSitecoreContext } from '@sitecore-jss/sitecore-jss-nextjs';
import Head from 'next/head';
import classNames from 'classnames';
// Lib
import { getThemeClasses, Theme } from 'lib/get-theme';
import { EnumField, getEnum } from 'lib/get-enum';

const PageLayout = (): JSX.Element => {
  const context = useSitecoreContext();

  // Fail out if we don't have any page data.
  if (!context.sitecoreContext.route) {
    return <></>;
  }

  const pageTheme =
    getEnum<Theme>(context?.sitecoreContext?.route?.fields?.pageTheme as EnumField<Theme>) ||
    'theme-black';
  const themeClasses = getThemeClasses(pageTheme);

  return (
    <>
      <Head>
        <title>JSS Training</title>
        <meta name="description" content="Page description" />
        <link rel="icon" href="/favicon.ico" />
        {/* Preload our two most heavily used webfonts */}
        <link
          rel="preload"
          href={`${process.env.PUBLIC_URL}/fonts/ModernEra-Black.woff2`}
          as="font"
          type="font/woff2"
          crossOrigin="anonymous"
        />
        <link
          rel="preload"
          href={`${process.env.PUBLIC_URL}/fonts/ModernEra-Regular.woff2`}
          as="font"
          type="font/woff2"
          crossOrigin="anonymous"
        />
        <style>{`
@font-face {
  font-display: swap;
  font-family: Modern Era;
  font-weight: 400;
  src: url(${process.env.PUBLIC_URL}/fonts/ModernEra-Regular.woff2) format('woff2'),
    url(/fonts/ModernEra-Regular.woff) format('woff');
}
@font-face {
  font-display: swap;
  font-family: Modern Era;
  font-weight: 700;
  src: url(${process.env.PUBLIC_URL}/fonts/ModernEra-Bold.woff2) format('woff2'),
    url(/fonts/ModernEra-Bold.woff) format('woff');
}
@font-face {
  font-display: swap;
  font-family: Modern Era;
  font-weight: 900;
  src: url(${process.env.PUBLIC_URL}/fonts/ModernEra-Black.woff2) format('woff2'),
    url(/fonts/ModernEra-Black.woff) format('woff');
}`}</style>
      </Head>
      <div className="flex flex-col min-h-screen">
        <header className={classNames(themeClasses, 'sticky', 'top-0', 'z-40')}>
          {/* Site header content */}
        </header>
        <main className={classNames(themeClasses, 'pb-20', 'flex-grow')}>
          {/* Site main content */}
        </main>
        <footer className={classNames(getThemeClasses('theme-black'))}>
          {/* Site foorter content */}
        </footer>
      </div>
    </>
  );
};

export default PageLayout;
