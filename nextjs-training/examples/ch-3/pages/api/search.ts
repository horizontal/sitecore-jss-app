// Interfaces
import type { NextApiRequest, NextApiResponse } from 'next';
import type { MockSearchArgs, MockSortBy } from '../../third-party/mock-api';
// Lib
import search from '../../lib/search-integraton';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === 'GET') {
    // Transform our query params into the arguments object
    const args: MockSearchArgs = {};

    if (req.query.sortBy) {
      args.sortBy = Array.isArray(req.query.sortBy)
        ? (req.query.sortBy[0] as MockSortBy)
        : (req.query.sortBy as MockSortBy);
    }

    return search(args)
      .then((response) => {
        res.status(200).send(response);
      })
      .catch((err) => {
        // Send an error if the API key isn't provided
        res.status(403).send(err);
      });
  }
};

export default handler;
