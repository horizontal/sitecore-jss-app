// Interfaces
export interface MockSearchResult {
  author: string;
  date: number;
  description: string;
  id: number;
  title: string;
}

export type MockSortBy = 'author' | 'date';

export interface MockSearchArgs {
  authors?: string | string[];
  query?: string;
  resultsPerPage?: number;
  sortBy?: MockSortBy;
}

interface MockSearchResponse {
  hits: MockSearchResult[];
  numPages: number;
  page: number;
}

// Mock data
const MOCK_TITLES = [
  'A beautiful little sunset',
  'All you need is a dream in your heart',
  'It just happens',
  'Just float around and be there',
  'See how easy it is',
  'You can create anything that makes you happy',
  "Let's make some happy little clouds in our world",
  'Sap Green',
  'Van Dyke Brown',
  'Burnt Umber',
  'Let your imagination be your guide',
  "I guess I'm a little weird",
];

const MOCK_AUTHORS = [
  'Louie Alvarez',
  'Scott Taylor',
  'Jasper Wolfe',
  'Hugo Moran',
  'Peter Shields',
  'Andrea Clay',
  'Britney Perkins',
  'Dena Newton',
  'Claudine Mayo',
];

const MOCK_DESCRIPTIONS = [
  'All you need is a dream in your heart, and an almighty knife. Just let go - and fall like a little waterfall. This is a happy place, little squirrels live here and play.',
  'Anything that you believe you can do strong enough, you can do. Anything. As long as you believe. A big strong tree needs big strong roots. The only thing worse than yellow snow is green snow.',
  'Let your imagination be your guide. You better get your coat out, this is going to be a cold painting. Think about a cloud.',
  'Life is too short to be alone, too precious. Share it with a friend. Use your imagination, let it go. You could sit here for weeks with your one hair brush trying to do that - or you could do it with one stroke with an almighty brush.',
  'Sometimes you learn more from your mistakes than you do from your masterpieces. This is the time to get out all your flustrations, much better than kicking the dog around the house or taking it out on your spouse.',
  "It's cold, but it's beautiful. We'll make some happy little bushes here. You are only limited by your imagination. A thin paint will stick to a thick paint.",
  "Without washing the brush, I'm gonna go right into some Van Dyke Brown, some Burnt Umber, and a little bit of Sap Green. In nature, dead trees are just as normal as live trees. The secret to doing anything is believing that you can do it.",
];

const MIN_DATE = 1633050000000;
const MAX_DATE = 1614387600000;
const DATE_DIFF = MAX_DATE - MIN_DATE;

const MOCK_RESULTS: MockSearchResult[] = [];

// Generate a set of search results that will remain static for this class instance
for (let i = 0; i < 50; i++) {
  const res = {
    title: MOCK_TITLES[Math.floor(Math.random() * MOCK_TITLES.length)],
    author: MOCK_AUTHORS[Math.floor(Math.random() * MOCK_AUTHORS.length)],
    description:
      MOCK_DESCRIPTIONS[Math.floor(Math.random() * MOCK_DESCRIPTIONS.length)],
    id: i,
    date: Math.floor(Math.random() * DATE_DIFF) + MIN_DATE,
  };

  MOCK_RESULTS.push(res);
}

// Class
class MockAPI {
  apiKey: string;

  constructor(apiKey: string) {
    this.apiKey = apiKey;
  }

  async search({
    query,
    authors,
    sortBy,
    resultsPerPage = 10,
  }: MockSearchArgs = {}): Promise<MockSearchResponse> {
    // Prevent searches if not initiialized
    if (!this.apiKey) {
      return Promise.reject('No API key provided.');
    }

    // This search is just an example, and a bad one at that :D
    let results = [...MOCK_RESULTS];

    if (!!query) {
      results = results.filter((r) => r.title.startsWith(query));
    }

    if (!!authors) {
      if (Array.isArray(authors)) {
        results = results.filter((r) => authors.includes(r.author));
      } else {
        results = results.filter((r) => r.author === authors);
      }
    }

    if (sortBy === 'date') {
      results.sort((a, b) => a.date - b.date);
    }

    if (sortBy === 'author') {
      results.sort((a, b) =>
        a.author < b.author ? -1 : a.author > b.author ? 1 : 0
      );
    }

    const numPages = Math.ceil(results.length / resultsPerPage);
    if (numPages > 1) {
      results = results.slice(0, resultsPerPage);
    }

    return Promise.resolve({ hits: results, numPages, page: 0 });
  }
}

export default MockAPI;
